# Phone HTML application for Tutorial

Deploy this app to your bluemix account to later use its sensor data in your NodeRED flows.

[![Deploy to Bluemix](https://bluemix.net/deploy/button.png)](http://bit.ly/2ezXKNh)

Please see the other repository for further instructions to use the generated data:
[NodeRED Repository](https://bitbucket.org/bmcrprojectteam/node-red-bluemix-iot-2016)